#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import requests
from bs4 import BeautifulSoup
from PIL import Image
try:
    import cookielib
except:
    import http.cookiejar as cookielib

# Built Request headers.
agent = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'
headers = {
    "Host": "readfree.me",
    "Referer": "http://readfree.me/accounts/login/?next=/",
    'User-Agent': agent
}

# Get the soup and try to fetch cookie.
all_url = 'http://readfree.me/accounts/login/?next=/'
s = requests.session()
s.cookies = cookielib.LWPCookieJar(filename='cookies')
try:
    s.cookies.load(ignore_discard=True)
except IOError:
    print('Cookie未加载！')


def get_csrf(soup):
    _csrf = soup.find('input', {'name': 'csrfmiddlewaretoken'})['value']
    return _csrf


# Show captcha image and input the code.
def get_captcha1(soup):
    curl = soup.find('img', {'class': 'captcha'})['src']
    captcha_url = 'http://readfree.me/captcha/image' + curl
    response = s.get(captcha_url, headers=headers)
    with open('captcha.jpg', 'wb') as f:
        f.write(response.content)
        f.close()
    im = Image.open('captcha.jpg')
    im.show()
    im.close()
    captcha = input('captcha code： ')
    return captcha


def get_captcha0(soup):
    captchacode = soup.find('input', {'name': 'captcha_0'})['value']
    return captchacode


def login(email, password):
    rq = s.get(url=all_url, headers=headers)
    soup = BeautifulSoup(rq.text, 'lxml')
    # Post the login imformation to the server.
    data = {
        'csrfmiddlewaretoken': get_csrf(soup),
        'email': email,
        'password': password,
        'captcha_1': get_captcha1(soup),
        'captcha_0': get_captcha0(soup),
    }
    result = s.post(all_url, data=data, headers=headers)
    s.cookies.save(ignore_discard=False, ignore_expires=False)


# Verfy the login by status code.
def is_login():
    if os.path.isfile('username.txt'):
        user_name = open('username.txt', 'r', encoding='utf-8').read()
    else:
        user_name = str(input('请输入你的用户名\n>  '))
        with open('username.txt', 'w', encoding='utf-8') as f:
            f.write(user_name)

    purl = 'http://readfree.me/accounts/profile/' + user_name + '/checkin/'
    login_code = s.get(
        purl, headers=headers, allow_redirects=False).status_code
    if login_code == 200:
        return True
    else:
        return False


def get_score():
    user_name = open('username.txt', 'r', encoding='utf-8').read()
    wish_url = 'http://readfree.me/accounts/profile/%s/wish/'%(user_name)
    rq = s.get(url=wish_url, headers=headers)
    soup = BeautifulSoup(rq.text, 'lxml')
    score = soup.findAll('p',{'class':'muted'})[-1].text.split(':')[-1]
    print('累计额度:',score)

def sign():
    sign_url = 'http://readfree.me/accounts/checkin'
    sign_code = requests.get(sign_url).status_code
    if sign_code == 200:
        print('签到成功')
        get_score()
    else:
        print('签到失败')


if __name__ == '__main__':
    if not is_login():
        print('登录失败')
        account = input('请输入你的邮箱\n>  ')
        secret = input('请输入你的密码\n>  ')
        login(account, secret)
    sign()
